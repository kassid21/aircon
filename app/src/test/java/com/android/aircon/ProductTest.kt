package com.android.aircon

import com.android.aircon.data.model.Product
import com.android.aircon.data.model.ProductSize
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*

class ProductTest {

    private var product: Product? = null

    @Before
    fun setup(){
        val productSize = ProductSize(55.0, 24.0, 32.0)

        product = Product("AirCon", "Fabulous AC", 250.0, productSize)
    }

    @Test
    fun checkVolumeOfTheProduct(){
        val product: Product = product!!

        val expectedResult = 0.04224

        val volume: Double = product.getVolume()
        assertEquals(expectedResult,volume,0.000001)
    }

    @Test
    fun checkWeighOfTheProduct(){
        val product: Product = product!!
        val volume = 0.0033800000000000006

        val expectedResult = 0.84500

        val weight: Double = product.getWeight(volume)
        assertEquals(expectedResult,weight,0.000001)
    }

    @After
    fun teardown(){
        product = null
    }
}