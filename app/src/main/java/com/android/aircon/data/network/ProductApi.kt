package com.android.aircon.data.network

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object ProductApi {

    private const val baseUrl = "http://wp8m3he1wt.s3-website-ap-southeast-2.amazonaws.com"

    private var instance: IProductApi? = null

    fun getInstance(): IProductApi{
        return instance ?: synchronized(this){
            instance ?: buildRetrofit().also {
                instance = it
            }
        }
    }

    private fun buildRetrofit(): IProductApi{

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(baseUrl)
            .build()
            .create(IProductApi::class.java)
    }
}