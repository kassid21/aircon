package com.android.aircon.data.model

data class ApiResponse(
    val next: String?,
    val objects: List<Product>
){

    /**
     * @return: Returns a single character, reflecting the api endpoint
     * or ""(empty string) for 'null' value
     */
    fun parseApiEndPoint(): String{
        next?.apply {
            return try {
                this.substring(14)
            }catch (e: Exception){
                ""
            }
        }
        return ""
    }
}