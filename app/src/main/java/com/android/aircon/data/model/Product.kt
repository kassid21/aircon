package com.android.aircon.data.model


data class Product(val category: String,
                   val title: String,
                   val weight: Double?,
                   val size: ProductSize
): IProduct {

    /**
     * @return: returns the volume of the given product
     */
    override fun getVolume(): Double =
        size.getHeightInMeters() * size.getLengthInMeters() * size.getWidthInMeters()

    /**
     * @param: takes the volume of the product
     * @return: Cubic weight of the product
     */
    override fun getWeight(volume: Double): Double {
        val conversionFactor = 250
        return volume * conversionFactor
    }
}