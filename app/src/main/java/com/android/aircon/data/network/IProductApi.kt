package com.android.aircon.data.network

import com.android.aircon.data.model.ApiResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface IProductApi {

    @GET("/api/products/{num}")
    fun getProducts(@Path("num") endpoint: String): Observable<ApiResponse>
}