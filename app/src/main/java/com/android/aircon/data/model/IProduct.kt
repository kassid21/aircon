package com.android.aircon.data.model

interface IProduct {

    fun getVolume(): Double
    fun getWeight(volume: Double): Double
}