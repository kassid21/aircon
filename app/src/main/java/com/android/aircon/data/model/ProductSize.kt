package com.android.aircon.data.model

data class ProductSize(private val length: Double,
                       private val height: Double,
                       private val width: Double){

    fun getLengthInMeters() = length/100.0

    fun getHeightInMeters() = height/100.0

    fun getWidthInMeters() = width/100.0
}
