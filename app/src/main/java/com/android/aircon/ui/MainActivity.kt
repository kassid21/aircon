package com.android.aircon.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.android.aircon.R
import com.android.aircon.data.model.ApiResponse
import com.android.aircon.data.model.Product
import com.android.aircon.data.network.IProductApi
import com.android.aircon.data.network.ProductApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    private lateinit var weightTextView: TextView
    private lateinit var fetchingTextView: TextView
    private lateinit var productApi: IProductApi

    private var compositeDisposable = CompositeDisposable()

    private var searchCategory = "Air Conditioners"

    // Refer to the first api endpoint.
    // This variable keeps on updating based on the response received
    private var endpoint = "1"
    private var totalWeight = 0.0
    private var numOfProducts = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        weightTextView = findViewById(R.id.actualweight)
        fetchingTextView = findViewById(R.id.fetchingtext)

        // Retrofit Client that gives concrete implementation of IProductApi Class
        productApi = ProductApi.getInstance()

        // Adding the entire sequential network call subscription to the disposable bag so as
        // to dispose the subscription and avoid memory leaks
        compositeDisposable.add(
            Observable.fromCallable {
                fetchData(endpoint) //Returns an observable of ApiResponse
            }.flatMap { observable ->
                observable.flatMap { response ->
                    // This block receives each endpoint json data as ApiResponse object

                    endpoint = response.parseApiEndPoint() // Parses json data, retrieves next api endpoint and stores in the variable
                    calculateAverageCubicWeight(response.objects,searchCategory)
                    Observable.just(endpoint)
                }
            }.repeatUntil {
                // Repeats the network requests until below condition is met
                endpoint == "" // Refers to 'next' in json data being null
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { endPoint ->
                    fetchingTextView.text = "Fetching from endpoint: $endPoint"
                },{ error ->
                    //This block is executed when an error occurs during network call
                    Toast.makeText(this@MainActivity,error.message,Toast.LENGTH_SHORT).show()
                },{
                    // This block is executed when all the network request are
                    // completed successfully
                    weightTextView.text = (totalWeight/numOfProducts).toString()
                    fetchingTextView.text = "Data successfully received"
                }
            )
        )
    }

    override fun onDestroy() {
        super.onDestroy()

        // Disposing the subscription to avoid memory leaks
        compositeDisposable.clear()
    }

    /**
     * @param: takes in the last character of the api endpoint
     * @return: An observable that can be subscribed to perform network request and fetch the result
     */
    private fun fetchData(endpoint: String): Observable<ApiResponse> =
        productApi.getProducts(endpoint)

    /**
     * @param: takes the list of products and the category to search for in the list
     * Calculates the Average cubic weight of the products that fall under given category
     */
    private fun calculateAverageCubicWeight(list: List<Product>?, category: String) {
        if(list == null || list.isEmpty()) return

        list.filter {
            it.category == category
        }.forEach { p ->
            totalWeight += p.getWeight(p.getVolume())
            numOfProducts += 1
        }
    }
}
