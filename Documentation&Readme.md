# AirCon

### An awesome app that displays the average cubic weight of all the Air Conditioners. This challenge has been completed in ***Kotlin*** programming language

&nbsp;

## Installation
---

### Download and Install [Android Studio](https://developer.android.com/studio)

### Unzip the project file

### Open Android Studio and click 'Open existing project'

### Point to the unzipped directory and proceed

### Install any missing files, emulators if prompted and click the Green Play Button to run the project in an emulator

&nbsp;

## Documentation for Class
---

### Product.kt

```
data class Product(val category: String,
                   val title: String,
                   val weight: Double?,
                   val size: ProductSize): IProduct {}
```

A class that provides user readable category, title, weight, size and also provides handy methods to calculate volume & cubic weight.

#### Public Methods

```
 getVolume()
 getWeight(volume: Double)
```

1. getVolume() : Returns the volume of the product based on the size provided in the constructor.

2. getWeight(volume: Double): Returns the cubic weight of the product based on given volume.

## Further Improvements

* Only Tested one class in the project. More Unit and UI Test can be added to the project.
* Due to the simplicity of the project, MVVM architecture pattern was not applied. Most of the business logic can be moved to new viewmodel class and data fetching to new repository class.



## Author

* [Mohammed Siddiq Kashif](kashif.msiddiq@gmail.com)

## Acknowledgments

Some of the third-party libraries used in the project


* [RxJava](https://github.com/ReactiveX/RxJava)


* [Gson](https://github.com/google/gson)

* [Retrofit](https://github.com/square/retrofit)
